/*
 * Name : client.c
 * Author : Tom Shen
 * Date : 2012/10/26
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <utils/Log.h>
#include <fcntl.h>

#include "cutils/properties.h"

#define LOG_TAG "Socket_client"

#define SERVER_IP         "192.168.88.140"
#define SERVER_PORT       80
//#define SERVER_IP         "192.168.169.170"
//#define SERVER_PORT       8080
#define SERVER_PARAMETER  "/CMPortal/NotifyFOTAStatus"

char* gen_request();
void sendToServer(char* imei) ;
void setWifitype();
 
int main(int argc, char* argv[]){
        char* imei;
        int i;
        int fd;
  
        fd = open("/data/misc/wifi/wpa_supplicant.conf",O_RDONLY);
        if (fd == -1) {
            LOGD("---- fail open /data/misc/wifi/wpa_supplicant.conf \n");
            return 0;
        }

        //setWifitype();
        for (i = 0; i < argc; i++) {
            printf("argv[%d] = %s \n", i, argv[i]);
            LOGD("argv[%d] = %s \n", i, argv[i]);
            switch (i) {
              case 1:
                  imei = argv[i];         
                  printf("-------------IMEI = %s\n",imei);
                  LOGD("-------------IMEI = %s\n",imei);
                  break;
              default:
                  break;
            }
        }

        sendToServer(imei);
        sendToServer(imei);

	return 0;
}

void sendToServer(char* imei) {
        int sockfd, numbytes;  
        int imei_index;
        FILE *fd;
	struct sockaddr_in address;
        const int timeout = 30;
        int now = 0;
        
        printf("---------------  start to send FOTA fail!!!   ----------------------\n");
 
	// TCP socket
	if ( ( sockfd = socket(AF_INET, SOCK_STREAM, 0) ) == -1 ){
		perror("socket");
                LOGD("TCP socket fail!!!");
		exit(1);
	}
 
	// Initial, connect to server port
	address.sin_family = AF_INET;
	address.sin_port = htons(SERVER_PORT);
	address.sin_addr.s_addr = inet_addr(SERVER_IP);
	bzero( &(address.sin_zero), 8 );

 printf("1---------------  start to send FOTA fail!!!   ----------------------\n");
 LOGD("1---------------  start to send FOTA fail!!!   ----------------------\n");
	// Connect to server
	while ( connect(sockfd, (struct sockaddr*)&address, sizeof(struct sockaddr)) == -1){
                if (now == 30) return;
                printf("---------------  connet to DMServer fail!!   ----------------------\n");
                LOGD("---------------  connet to DMServer fail!!   ----------------------\n");
		perror("connect");
		//exit(1);
                sleep(2);
                now = now + 2;
	}
 printf("2---------------  start to send FOTA fail!!!   ----------------------\n");
 LOGD("2---------------  start to send FOTA fail!!!   ----------------------\n");
       /* 
       if (imei == NULL) {
           imei = (char*)malloc(sizeof(char) * 256);
           fd = fopen("/sys/class/net/wlan0/address", "ro");
           fread(imei, 256, 1, fd);
           printf("2.IMEI = %s\n",imei);
       }*/
        
       char* text = gen_request(imei);
       /* send it !!! */
       write(sockfd, text, strlen(text) + 1);

 printf("5---------------  start to send FOTA fail!!!   ----------------------\n");
 LOGD("5---------------  start to send FOTA fail!!!   ----------------------\n");

	close(sockfd);
}

char* gen_request(char* imei) {
   char* text = (char*)malloc(sizeof(char) * 4096);

   memset(text, 0, sizeof(char) * 4096);
   /* text packet get from wireshark */
   strcat(text, "POST ");
   strcat(text, SERVER_PARAMETER);
   strcat(text, " HTTP/1.1\r\n");
   strcat(text, "Host: ");
   strcat(text, SERVER_IP);
   strcat(text, " \r\n");
   strcat(text, "User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.3) Gecko/20061201 Firefox/2.0.0.3 (Ubuntu-feisty)\r\n");
   strcat(text, "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n");
   strcat(text, "Accept-Language: zh-cn,zh;q=0.5\r\n");
   strcat(text, "Accept-Encoding: gzip,deflate\r\n");
   strcat(text, "Accept-Charset: gb2312,utf-8;q=0.7,*;q=0.7\r\n");
   strcat(text, "Keep-Alive: 300\r\n");
   strcat(text, "Connection: keep-alive\r\n");
   strcat(text, "Referer: http://");
   strcat(text, SERVER_IP);
   strcat(text, " \r\n");
//   strcat(text, "Cookie: usrtime=1178023651; lasturl=http%3A%2F%2Fwww.su.zju.edu.cn%2Fhack%2Fvisit.php%3Ftype%3D1; loginurl=http%3A%2F%2Fwww.su.zju.edu.cn%2Fhack%2Fvote.php%3Ftype%3D2; usrsid=XgVB1KO7mVEanJwLbwJ0lqXBSHErTAcN; usripfrom=Unknow; usrtime=1178023650; lasturl=http%3A%2F%2Fwww.su.zju.edu.cn%2F\r\n");
   strcat(text, "Content-Type: application/x-www-form-urlencoded\r\n");
   strcat(text, "Content-Length: 200\r\n");
   strcat(text, "\r\n");
   //strcat(text, "IMEI=");
   strcat(text, imei);

   return text;
} 

void setWifitype() {
   int fOut;
   char wifitype[1];

   system("/system/bin/wifimacwriter");
   property_get("wifi.module.type",wifitype,"0");

   //Create a new file
   fOut = open ("/data/wifiType", O_RDWR | O_CREAT | O_SYNC);
   printf("wifi type = %s\n",wifitype);
   LOGD("wifi type = %s\n",wifitype);


   //write to it and close it.
   write (fOut, wifitype, sizeof(wifitype)/sizeof(char));
   close (fOut);
   
   return;
}
