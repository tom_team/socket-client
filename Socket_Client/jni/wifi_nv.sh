#!/bin/sh -x
#wifi on
insmod /system/lib/modules/bcm4329.ko firmware_path=/vendor/firmware/fw_bcm4329.bin nvram_path=/data/misc/wifi/nvram.txt iface_name=wlan0
cd /data/misc/wifi
# -D = driver name (can be multiple drivers: nl80211,wext)
wpa_supplicant -iwlan0 -Dnl80211,wext -c/data/misc/wifi/wpa_supplicant.conf -e/data/misc/wifi/entropy.bin -B

while [ 1 ];
do
   sleep 1
   /system/bin/dhcpcd -ABKL -d wlan0
    echo "wait 1 second....."
done

